import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { TaskModel } from "../task/task";

/**
 * Model description here for TypeScript hints.
 */
export const ModelTasksModel = types
  .model("ModelTasks")
  .props({
    // Tasks: types.optional(types.frozen(), { activeDate: "", Data: "" }),
    data : types.optional(types.frozen(), {activeDate: '', tasks: []}),
    newObj: types.optional(types.frozen(),{}),




  })
  .views((self) => ({

    // get GetTasks() {
    //   return self.tasksModel.task
    // },


    get GetactiveDate() {
      return self.data.tasks.activeDate
    },

     GetTasksByDate (date) {

      return  self.data.tasks.filter((item ) => item.date === date);
    },
    get GetTasks () {
      return self.data.tasks;
  }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({

    setTaskByDate (data){

      const taskObj = self.data.tasks.filter((item) => item.date === data.date);
      let newObj
  
      if (!taskObj[0]?.tasks?.length){
         newObj = {
          date:  data.date,
          tasks: [data.task],
      }
      console.log([... self.data.tasks.filter(item => item.date !== data.date), self.newObj], "1");
      
          self.data = {activeDate: self.data.activeDate, tasks: [... self.data.tasks.filter(item => item.date !== data.date), newObj]};
      // console.log({newObj}, self.data);
          return;
      }
  
   
         
       newObj = {
        date: data.date,
        tasks: [...taskObj[0].tasks, data.task],
    }
    self.data = {activeDate: self.data.activeDate, tasks: [... self.data.tasks.filter(item => item.date !== data.date), newObj]};
      // console.log({newObj, data:self.data});
  }

  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type ModelTasksType = Instance<typeof ModelTasksModel>
export interface ModelTasks extends ModelTasksType { }
type ModelTasksSnapshotType = SnapshotOut<typeof ModelTasksModel>
export interface ModelTasksSnapshot extends ModelTasksSnapshotType { }
export const createModelTasksDefaultModel = () => types.optional(ModelTasksModel, {})
