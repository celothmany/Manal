import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */


export const TaskModel = types
  .model("Task")
  .props({
    taskTitle: types.optional(types.frozen(),{date: "", tasks:[]}),
    taskDate: types.optional(types.frozen(),[]),
    taskStatus: types.optional(types.boolean, false),
  })
  .views((self) => ({
    get GetTaskTitle() {
      return self.taskTitle
    },
    get GetTaskDate() {
      return self.taskDate
    },
    get GetTasksStatus() {
      return self.taskStatus
    },
    get GetTasks() {
      return self.taskTitle
    },


  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setTaskTitle(data) {
      self.taskTitle = data
    },

    setTaskDate(value: string[]) {
      self.taskDate = value
    },

    setTaskStatus(value: boolean) {
      self.taskStatus = value
    },

  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type TaskType = Instance<typeof TaskModel>
export interface Task extends TaskType { }
type TaskSnapshotType = SnapshotOut<typeof TaskModel>
export interface TaskSnapshot extends TaskSnapshotType { }
export const createTaskDefaultModel = () => types.optional(TaskModel, {})
