import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { Image, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Header, Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { ScrollView } from "react-native-gesture-handler"
import { SafeAreaView } from "react-native-safe-area-context"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"


const menu = require("../../../assets/images/Cartot_menu.png")
const addButton = require("../../../assets/images/addButton.png")
const notif = require("../../../assets/images/notif.png")


const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

export const TasksScreen = observer(function TasksScreen() {
  // Pull in one of our MST stores
  const { TasksUserStore } = useStores();

  const [value, setValue] = useState("Today");
  
  const [active, setActive] = useState("Today");

  const handelClick = (value) => {
    setValue(value);
    setActive(value);
  }
  


  // Pull in navigation via hook
  const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="scroll">
      <SafeAreaView style={ConatinerNew}>
        <View style={HeaderNew}>
          <Image source={menu} style={imageheader}></Image>
        </View>
        <View style={Tasks}>
          <Text style={TextTitle}>{"Tasks"}</Text>
          <Image source={notif} />
        </View>
        <View style={TaskItems}>
          <View style={Left}>
            <Text onPress={() => handelClick("Today")} style={{ transform: [{ rotate: "-90deg" }],  letterSpacing: 3, color : active === "Today" ? 'green' : 'black'}}>{"TODAY"}</Text>
            <Text onPress={() => handelClick("Tomorrow")} style={{ transform: [{ rotate: "-90deg" }],  letterSpacing: 3, color : active === "Tomorrow" ? 'green' : 'black'}}>{"TOMORROW"}</Text>
            <Text onPress={() => handelClick("Select Date")} style={{ transform: [{ rotate: "-90deg" }],  letterSpacing: 3, color : active === "Select Date" ? 'green' : 'black'}}>{"THIS WEEK"}</Text>
          </View>
          <ScrollView style={Right}>
            {TasksUserStore.GetTasksByDate(value).map((allTask: any, i: any) =>
              allTask.tasks.map((item, index) => {
                return (
                  <Text key={index} style={textStyle}>{item}</Text>
                )
              })
            )
            }
          </ScrollView>
          <TouchableOpacity onPress={() => navigation.navigate("addnew")}>
            <View style={addTask}>
              <Image source={addButton}></Image>
            </View>
          </TouchableOpacity >
        </View>

      </SafeAreaView>
    </Screen>
  )
})

const ConatinerNew: ViewStyle = {
  backgroundColor: "white",
  display: "flex",
  flex: 1
}
const HeaderNew: ViewStyle = {


}
const imageheader: ViewStyle = {
  width: 40,
  height: 15,
  margin: 30
}

const Tasks: ViewStyle = {
  display: 'flex',
  flexDirection: 'row',
  margin: 30,
  justifyContent: "space-between",
  alignItems: "center"

}
const TextTitle: TextStyle = {
  color: "black",
  fontSize: 50,
}
const textStyle: TextStyle = {
  color: "black",
  // height: 70,
  width: "100%",
  // lineHeight:100,
  textAlign: "left",
  fontSize: 30,
  paddingLeft: 20,
  marginBottom: 20
}
const TaskItems: ViewStyle = {
  display: "flex",
  flexDirection: "row",
  flex: 1,
  // margin: 10,
  // maxHeight: "100%",
  // backgroundColor: "blue",
}
const Left: ViewStyle = {
  // width: "20%",
  // backgroundColor: "yellow",
  alignItems: "center",
  marginLeft: -20,
  justifyContent: "space-evenly",
  height: "90%",
}
const Right: ViewStyle = {
  backgroundColor: "rgba(212, 212, 212, 0.3)",
  marginLeft: -10,
  width: "85%",
  display: "flex",
  maxHeight: "100%",
  flexDirection: "column",
  paddingRight: 15,
  position: "relative",
}
// const Rightitems: TextStyle = {
//   transform: [{ rotate: "-90deg" }],
//   letterSpacing: 3,
//   color: "black",
// }
const addTask: ViewStyle = {
  position: "absolute",
  right: 0,
  bottom: 0,
  height: 100,
  width: 115,
  backgroundColor: "#FE4775",
  borderTopLeftRadius: 5,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
}