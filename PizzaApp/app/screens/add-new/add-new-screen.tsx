import React, { useState } from "react"
import { observer } from "mobx-react-lite"
import { Image, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import metrics from "../../theme/metrics"
import { ScrollView, TextInput } from "react-native-gesture-handler"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"
import { array } from "mobx-state-tree/dist/internal"

const addButton = require("../../../assets/images/addButton.png")


const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
  position: "relative",
}

export const AddNewScreen = observer(function AddNewScreen() {

  const { TaskStore, TasksUserStore } = useStores();
  const [tasktitle, setTaskTitle] = useState("");
  const [day, setDay] = useState("");
  const [active, setActive] = useState("");

  const handelClick = (value) => {
    setDay(value);
    setActive(value);
  }




  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="scroll">
      <View style={Header}>
        <Text style={Tasks}>{"Add Tasks"}</Text>
      </View>
      <View style={INputNew}>
        <TextInput style={Input} placeholder="Add New"
          onChangeText={(text) => {
            setTaskTitle(text)
          }}>

        </TextInput>
      </View>
      <View style={Content}>
        <Text style={{ color: 'black' }}>{"WHEN"}</Text>
        <View style={TitleContent}>
          <Text onPress={() => handelClick("Today")} style={{ color: active === "Today" ? 'green' : "black" }}>{"Today"}</Text>
          <Text onPress={() => handelClick("Tomorrow")} style={{ color: active === "Tomorrow" ? 'green' : "black" }}>{"Tomorrow"}</Text>
          <Text onPress={() => handelClick("Select Date")} style={{ color: active === "Select Date" ? 'green' : "black" }}>{"Select Date"}</Text>
        </View>
        <ScrollView>
        {
          TasksUserStore.GetTasks.map((allTask: any, i: any) => 
          allTask.tasks.map((item, index) => {
            return (
              <View  key={index} style={{marginLeft: 30}}>
                <Text style={taskcontent}>{item}</Text>
                <Text style={{ color: 'green' }}>{allTask.date} </Text>

              </View>
            )
          })
          
          )

        }
        </ScrollView>
        {/* <Text style={{ color: 'green' }}>{TasksUserStore.GetTasks[1].date} </Text> */}
      </View>
      <View style={Footer}>
        <TouchableOpacity onPress={async () => {
          console.log("tasks =>:", TasksUserStore.GetTasksByDate('Today'), 'hi');

          TasksUserStore.setTaskByDate({ date: day, task: tasktitle })
          // TaskStore.setTaskDate([...TaskStore.GetTaskDate, day])
          // console.log(TasksUserStore.GetTasks[0].tasks, "1");


        }}>
          {/* <Image source={addButton}></Image> */}
          <Text>Add Task</Text>
        </TouchableOpacity>
      </View>
    </Screen>
  )
})

const Header: ViewStyle = {
  display: 'flex',
  flexDirection: 'row',
  margin: metrics.heightPercentageToDP(5),
  justifyContent: "space-between",
  alignItems: "center",
}
const Tasks: TextStyle = {
  color: 'black',
  fontSize: metrics.heightPercentageToDP(5)
}
const INputNew: ViewStyle = {
  display: 'flex',
  flexDirection: 'row',
  margin: metrics.heightPercentageToDP(5),
  alignItems: "center",
}
const Input: TextStyle = {
  height: metrics.heightPercentageToDP(10),
  borderBottomWidth: metrics.widthPercentageToDP(1),
  borderBottomColor: 'gray',
  padding: metrics.heightPercentageToDP(1),
  width: metrics.widthPercentageToDP(100),
  fontSize: metrics.heightPercentageToDP(4)

}
const Footer: ViewStyle = {
  backgroundColor: "#FE4775",
  position: "absolute",
  bottom: metrics.heightPercentageToDP(0),
  width: metrics.widthPercentageToDP(100),
  alignItems: "center",
  padding: metrics.heightPercentageToDP(2)
}
const Content: ViewStyle = {
  display: 'flex',
  marginStart: metrics.heightPercentageToDP(5),
}
const TitleContent: ViewStyle = {
  display: "flex",
  width: "100%",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-evenly",
  margin: metrics.heightPercentageToDP(2),
  padding: metrics.heightPercentageToDP(2),
  backgroundColor: 'rgb(212, 212, 212)',
  borderBottomLeftRadius: metrics.heightPercentageToDP(0.3),
  borderTopLeftRadius: metrics.heightPercentageToDP(0.3),

}
const taskcontent: TextStyle = {
  color: 'black',
  fontSize: metrics.heightPercentageToDP(4)

}