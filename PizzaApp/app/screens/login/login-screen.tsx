import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import { Dimensions, Image, ImageBackground, ImageStyle, TextStyle, View, ViewStyle } from "react-native"
import { Screen, Text } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { TouchableOpacity } from "react-native-gesture-handler"
import metrics from "../../theme/metrics"

import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-google-signin/google-signin';

const backgroundImage = require("../../../assets/images/test.png")
const google = require("../../../assets/images/google.png")


const ROOT: ViewStyle = {
  backgroundColor: color.palette.black,
  flex: 1,
}

export const LoginScreen = observer(function LoginScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  useEffect(() => {
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '874940426970-r0id74hidm0e2vmi9mekpefdppadbicr.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      // hostedDomain: '', // specifies a hosted domain restriction
      forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      // accountName: '', // [Android] specifies an account name on the device that should be used
      // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
      googleServicePlistPath: '', // [iOS] if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
      openIdRealm: '', // [iOS] The OpenID2 realm of the home web server. This allows Google to include the user's OpenID Identifier in the OpenID Connect ID token.
      profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
    });
  }, [])

  const signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
     GoogleSignin.signIn().then((userInfo)=>{

      console.log({ userInfo });
      navigation.navigate("tasks");
      }).catch((execption)=>{console.log(execption);
      });

      // this.setState({ userInfo });

    } catch (error) {
      console.log({ error });

      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };
  return (
    <Screen style={ROOT} preset="scroll">
      <View style={Container} >
        <View style={ContainerImage}>

          <ImageBackground
            resizeMode="cover"
            source={backgroundImage}
            style={BACKGROUNDSCREEN}

          >
          </ImageBackground>

        </View>
        <View style={Content}>
          <Text style={Title}>Hello there,
            Welcome </Text>

          <View style={loginContainer}>
            <Text style={textLogin}>Sign In With Google</Text>
            <TouchableOpacity onPress={() => signIn()}>
              <View style={imageLogin}>
                <Image source={google}></Image>

              </View>
            </TouchableOpacity>
          </View>

           
        </View>
      </View>
    </Screen>
  )
})


const Container: ViewStyle = {
  flex: 1,
  backgroundColor: "#FE4775",
}
const ContainerImage: ViewStyle = {
  height: 600,
  width: Dimensions.get('window').width,
  overflow: "hidden"
}
const BACKGROUNDSCREEN: ImageStyle = {
  display: "flex",
  alignContent: "center",
  justifyContent: "center",
  height: Dimensions.get('window').height * 1.2,
  width: Dimensions.get('window').width,
  position: 'absolute',
  right: '35%',
  top: "-40%"
}
const Content: ViewStyle = {
  display: "flex",
  position: 'absolute',
  left: '15%',
  top: "43%",
  right: 0,
}
const Title: TextStyle = {
  fontSize: 40,
  width: 300,
  marginBottom: metrics.heightPercentageToDP(16),
}
const loginContainer: ViewStyle = {
  display: "flex",
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: "center",
}
const textLogin: TextStyle = {
  fontSize: 20,
  fontWeight: "bold",
  textAlign: 'center',
  flex: 1,
}
const imageLogin: ViewStyle = {
  backgroundColor: "white",
  width: 100,
  height: 70,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  borderTopLeftRadius: 10,
  borderBottomLeftRadius: 10
}