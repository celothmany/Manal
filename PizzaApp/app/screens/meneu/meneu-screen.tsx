import React from "react"
import { observer } from "mobx-react-lite"
import { Image, TextStyle, View, ViewStyle } from "react-native"
import { Screen, Text } from "../../components"
// import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import metrics from "../../theme/metrics"

const fermer = require("../../../assets/images/fermer.png")


const ROOT: ViewStyle = {
  backgroundColor:'#161060',
  flex: 1,
}

export const MeneuScreen = observer(function MeneuScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  // const navigation = useNavigation()
  return (
    <Screen style={ROOT} preset="scroll">
      <View style={Header}>
        <Text style={TextHeader}>TASK APP</Text>
        <Image source={fermer} ></Image>
      </View>
      <View style={ContentMeneu}>
        <Text style={TextContent}>Dashboard </Text>
        <Text style={TextContent}>Calender</Text>
        <Text style={TextContent}>Categorize Tasks</Text>
        <Text style={TextContent}>Setting</Text>
      </View>
      <View style={footerMeneu}>
        <Text style={footerText}>About </Text>
        <Text style={footerText}>Privacy Policy</Text>
        <Text style={footerText}>License</Text>
      </View>
    </Screen>
  )
})


const Header: ViewStyle={
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-between",
  margin: metrics.heightPercentageToDP(4)
}
const TextHeader: TextStyle = {
  fontSize: metrics.heightPercentageToDP(2),
  fontWeight: "bold"
}
const ContentMeneu: ViewStyle={
  margin: metrics.heightPercentageToDP(4),
}
const TextContent: TextStyle={
  fontSize: metrics.heightPercentageToDP(3),
  paddingTop: metrics.heightPercentageToDP(2),
  fontWeight: "500"
}
const footerMeneu: ViewStyle={
  
  margin: metrics.heightPercentageToDP(4),
}
const footerText: TextStyle = {
  fontSize: metrics.heightPercentageToDP(2),
  paddingTop: metrics.heightPercentageToDP(2)
}